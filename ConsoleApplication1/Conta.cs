﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Conta
    {
        public void operaSaida(string simbolo, decimal valorSaida, Historico saldo, List<Historico> lista)
        {
            if (simbolo == "R$")
            {
                saldo.Valor = saldo.Valor - valorSaida;
                var operacao = new Historico()
                {
                    Valor = valorSaida,
                    Data = DateTime.Now,
                    Simbolo = simbolo,
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }
            if (simbolo == "$")
            {
                saldo.Valor = saldo.Valor - valorSaida;
                var operacao = new Historico()
                {
                    Valor = valorSaida,
                    Data = DateTime.Now,
                    Simbolo = simbolo,
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }                     
        }
    
        public void operaEntrada(string simbolo, decimal valorEntrada, Historico saldo, List<Historico> lista)
        {           
            if (simbolo == "R$")
            {
                saldo.Valor = valorEntrada + saldo.Valor;
                var operacao = new Historico()
                {
                    Valor = valorEntrada,
                    Data = DateTime.Now,
                    Simbolo = simbolo,
                    TipoOperacao = "Entrada"
                };
                lista.Add(operacao);
            }

            if (simbolo == "$")
            {
                saldo.Valor = valorEntrada + saldo.Valor;
                var operacao = new Historico()
                {
                    Valor = valorEntrada,
                    Data = DateTime.Now,
                    Simbolo = simbolo,
                    TipoOperacao = "Entrada",
                    Cotacao = saldo.Cotacao
                };
                lista.Add(operacao);
            }          
        }

        public decimal checaSaldo(string tipo, Historico meuSaldoReal, Historico meuSaldoDollar)
        {
            if (tipo == "$")
                return meuSaldoDollar.Valor;
            else
                return meuSaldoReal.Valor;
        }
    }
}
